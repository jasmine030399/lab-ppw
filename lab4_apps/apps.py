from django.apps import AppConfig


class Lab4AppsConfig(AppConfig):
    name = 'lab4_apps'
