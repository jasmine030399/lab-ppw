from django.conf.urls import url
from .views import index
from .views import bio
from .views import resume
from .views import writing
from .views import photography
from .views import register
from .views import form
app_name = 'lab4_apps'
urlpatterns = [
 url(r'^$', index, name ='index'),
 url(r'^bio/', bio, name ='bio'),
 url(r'^resume/', resume, name ='resume'),
 url(r'^writing/', writing, name ='writing'),
 url(r'^photography/', photography, name ='photography'),
 url(r'^register/', register, name ='register'),
 url(r'^form/', form, name='form'),
]
