from django.db import models
from django.utils import timezone
from datetime import datetime, date



# Create your models here.
class Person(models.Model):
    name = models.CharField(max_length=30)
    kategori = models.CharField(max_length=30)
    hari = models.CharField(max_length=30)
    tanggal = models.DateField()
    jam = models.TimeField()
    tempat = models.CharField(max_length=40)

    def __str__(self):
        return self.name

class Jadwal(models.Model):
    author = models.ForeignKey(Person, on_delete = models.CASCADE)
    content = models.CharField(max_length=125)
    published_date = models.DateTimeField(default=timezone.now)

