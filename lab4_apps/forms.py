from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit
from django import forms
from .models import Person


class KegiatanForm(forms.Form):

        nama_kegiatan = forms.CharField(max_length=30)
        hari = forms.CharField(max_length=30)
        tanggal= forms.DateField(widget=forms.SelectDateWidget())
        jam = forms.TimeField()
        tempat = forms.CharField(max_length=40)
        kategori = forms.ChoiceField(choices=[('rapat', 'Rapat'), ('belajar', 'Belajar')])


        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.helper = FormHelper
            self.helper.form_method = 'post'

            self.helper.layout = Layout(
                
                'nama_kegiatan',
                'hari',
                'tanggal',
                'jam',
                'tempat',
                'kategori',
##                Submit('submit', 'Submit', css_class='btn-success')
            )
